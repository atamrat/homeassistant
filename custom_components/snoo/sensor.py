"""Snoo platform that has a sleep sensor."""
from homeassistant.helpers.entity import Entity

from . import DOMAIN


async def async_setup_platform(hass, config, async_add_entities, discovery_info=None):
    """Set up the Snoo sensors."""
    client = hass.data[DOMAIN]['snoo_client']

    async_add_entities(
        [
            SnooSensor(
                client,
                "Snoo Sleep Sensor",
                None,
            )
        ]
    )


async def async_setup_entry(hass, config_entry, async_add_entities):
    """Set up the Snoo config entry."""
    await async_setup_platform(hass, {}, async_add_entities)


class SnooSensor(Entity):
    """Representation of a Snoo sensor."""

    def __init__(
        self, client, name, state
    ):
        """Initialize the sensor."""
        self._client = client
        self._name = "snoo"
        self._state = state


    @property
    def name(self):
        """Return the name of the sensor."""
        return self._name

    @property
    def state(self):
        """Return the state of the sensor."""
        return self._state

    def update(self):
        """Fetch new state data for this snoo
        This is the only method that should fetch new data for Home Assistant
        """
        session = self._client.get_current_session()

        if session['end_time']:
            self._state = "Awake"
        elif session["level"] == "BASELINE":
            self._state = "Asleep"
        else:
            self._state = "Soothing"