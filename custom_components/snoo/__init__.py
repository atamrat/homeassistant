"""Example Load Platform integration."""
from homeassistant.const import CONF_FILE_PATH
import homeassistant.helpers.config_validation as cv
import voluptuous as vol
import logging
DOMAIN = 'snoo'

_LOGGER = logging.getLogger(__name__)

# Validation of the user's configuration
CONFIG_SCHEMA = vol.Schema({
    DOMAIN: vol.Schema({
        # TODO: Make Constant
        vol.Optional("snoo_config_file"): cv.string,
    })
}, extra=vol.ALLOW_EXTRA)


def setup(hass, config):
    """Your controller/hub specific code."""

    from snoo import Client
    # Assign configuration variables.
    # The configuration check takes care they are present.

    conf = config[DOMAIN]
    
    # maybe set config file here
    client = Client()

    hass.data[DOMAIN] = {
        'snoo_client': client
    }

    hass.helpers.discovery.load_platform('sensor', DOMAIN, {}, config)

    return True
